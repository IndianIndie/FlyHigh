﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 displacement = new Vector3(10f, 10f,10f);
    [SerializeField] float timeToComplete = 2f; // 2 Secs

    Vector3 startingLocation;

    // Start is called before the first frame update
    void Start()
    {
        startingLocation = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToComplete <= Mathf.Epsilon) { return; }

        // IN 2 Pi, one cycle completes, 
        // Time at multiples of timeToComplete will be 1,2,.. 
        // Causing to have 2Pi after timeToComplete amount of time
        float rawSineWave = Mathf.Sin(2 * Mathf.PI * Time.time / timeToComplete);
        // To have range from 0 to 1, instead of -1 to 1
        float displacementScale = rawSineWave / 2 + 0.5f;
        transform.position = startingLocation + displacement * displacementScale;
    }
}
