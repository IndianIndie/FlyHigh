﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlyingComponentScript : MonoBehaviour
{
    enum State { Alive, Transcending, Dead }
    State state;
    Rigidbody rigidbody;
    AudioSource audioSource;
    bool debugGodMode = false;
    [SerializeField] float jumpSpeed = 100f;
    [SerializeField] float rotateSpeed = 100f;
    [SerializeField] AudioClip engine;
    [SerializeField] AudioClip success;
    [SerializeField] AudioClip death;
    [SerializeField] ParticleSystem engineParticle;
    [SerializeField] ParticleSystem successParticle;
    [SerializeField] ParticleSystem deathParticle;
    [SerializeField] float levelLoadDelay = 1f;
    [SerializeField] float deathLoadDelay = 2f;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (state != State.Alive) { return; } // Do not process if not alive

        foreach (ContactPoint contact in collision.contacts)
        {
            switch (collision.gameObject.tag)
            {
                case "Friendly":
                    // Do nothing
                    break;
                case "Finish":
                    StartNextLevelSequence();
                    break;
                default:
                    if (!debugGodMode)
                        StartDeathSequence();
                    break;
            }
        }
    }

    private void StartNextLevelSequence()
    {
        state = State.Transcending;
        audioSource.Stop();
        audioSource.PlayOneShot(success);
        Invoke("LoadNextScene", levelLoadDelay);
        if (engineParticle.isPlaying) { engineParticle.Stop(); }
        successParticle.Play();
    }

    private void StartDeathSequence()
    {
        state = State.Dead;
        audioSource.Stop();
        audioSource.PlayOneShot(death);
        Invoke("ReloadScene", deathLoadDelay);
        if (engineParticle.isPlaying) { engineParticle.Stop(); }
        deathParticle.Play();
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int totalScenes = SceneManager.sceneCountInBuildSettings;
        int nextSceneIndex = (currentSceneIndex + 1) % totalScenes;
        SceneManager.LoadScene(nextSceneIndex);
    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.Alive)
        {
            HandleJump();
            HandleSteer();
            if (Debug.isDebugBuild)
            {
                HandleDebugKeys();
            }
        }
    }

    private void HandleDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextScene();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            debugGodMode = !debugGodMode;
        }
    }

    private void HandleSteer()
    {
        rigidbody.angularVelocity = Vector3.zero; // stop the momentum from Physics
        float speedNow = rotateSpeed * Time.deltaTime;
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward * speedNow);
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * speedNow);
        }
    }

    private void HandleJump()
    {
        // Handle Jump: Can handle it alongside any steer
        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            if (rigidbody) rigidbody.AddRelativeForce(Vector3.up * jumpSpeed);

            // While jumping, play the engine sound if not playing
            if (!audioSource.isPlaying) { audioSource.PlayOneShot(engine); }
            if (!engineParticle.isPlaying) { engineParticle.Play(); }
        }
        else
        {
            if (audioSource.isPlaying) { audioSource.Pause(); }
            if (engineParticle.isPlaying) { engineParticle.Stop(); }
        }
    }
}
